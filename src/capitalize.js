/**
 * capitalizes a given word
 *
 */

module.exports = function capitalize(word) {
    if ((/^[a-zA-Z]+$/.test(word) !== true)) {
        throw new Error(/bad input/);
    }
    const firstLetter = word[0].toUpperCase();
    const capitalized = firstLetter + word.slice(1,);
    return capitalized;
    
};