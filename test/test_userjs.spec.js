const contact = require('../src/user');


it('expect snapshot to match UserDataId(1)', () => {
    const person = contact(1);
    expect(person).toEqual(contact(1));
});

it('expect snapshot to match UserDataId(56)', () => {
    const person = contact(56);
    expect(person).toEqual(contact(56));
});

it('expect snapshot to match UserDataId(1345)', () => {
    const person = contact(1345);
    expect(person).toEqual(contact(1345));
});

it('expect snapshot to throw error: id needs to be integer.', () => {
    const text = 'error';
    expect(() => {
        contact(text);
    }).toThrow('id needs to be integer');
});