const capitalize = require('../src/capitalize');

describe('capitalize', () => {
    it('triin => Triin', () => {
        expect(capitalize('triin')).toBe('Triin');
    });
    it('error - bad input', () => {
        expect(() => {
            capitalize({ 'word': 'cat' });
        }).toThrow(/bad input/);
    });
});
